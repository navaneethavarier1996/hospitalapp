import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test6 {

    public Test6() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPhoneNumberValidity() {
        HospitalApp h = new HospitalApp();
        int r = h.phoneNumberValidity("131243254");
        assertEquals(1, r);
    }

}
