
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test2 {
    Patient p = new Patient();

    public Test2() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPatientID() {
        MyDatabase db = new MyDatabase();
        // db.addPatient();
        int res = p.validatePatient(1, db);
        assertEquals(1, res);
    }
}
