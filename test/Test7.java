import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test7 {
    Timetable t = new Timetable(2, "Consultation", "Nandana", "Osteopathy", "03-05-2021 7:30-8:00", "Consultation Room",
            "Available");

    public Test7() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSlotStatusChange() {
        MyDatabase db = new MyDatabase();
        t.setAppointmentStatus("Booked");
        db.timetableDB.add(t);

        String res;
        res = db.timetableDB.get(140).getAppointmentStatus();
        assertEquals("Booked", res);
    }
}
