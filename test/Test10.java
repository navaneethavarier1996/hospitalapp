import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test10 {
    Appointment a = new Appointment("Divya", "Visitor", 1);

    public Test10() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkAppointment() {
        String res;
        res = a.getUserType();
        assertEquals("Visitor", res);
    }
}
