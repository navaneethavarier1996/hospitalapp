
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test1 {
    Patient p = new Patient("Nayana", 16, "75667587");

    public Test1() {

    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPatientDetails() {
        String res;
        res = p.getName();
        assertEquals("Nayana", res);
    }
}
