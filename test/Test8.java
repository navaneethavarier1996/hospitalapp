import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test8 {
    Physician phy = new Physician(1, "Surya", "Hatfield", new String[] { "Surgery" }, 54645756, "Monday 7:00-9:00");

    public Test8() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkPhysicianValues() {
        String res;
        res = phy.getPhyAddress();
        assertEquals("Hatfield", res);
    }
}
