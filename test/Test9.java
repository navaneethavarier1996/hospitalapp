import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test9 {
    AreaOfExpertise a = new AreaOfExpertise("Meditation therapy");

    public Test9() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkExpertiseValues() {
        String res;
        res = a.getExp();
        assertEquals("Meditation therapy", res);
    }
}
