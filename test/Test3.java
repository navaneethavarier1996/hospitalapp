
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test3 {
    Timetable t = new Timetable();

    public Test3() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSlotIDValidity() {
        MyDatabase db = new MyDatabase();
        // db.addTimetable();
        int res = t.validateSlotID(2, db);
        assertEquals(1, res);
    }
}
