
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test4 {

    public Test4() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAreaOfExpertise() {
        MyDatabase db = new MyDatabase();
        AreaOfExpertise a = new AreaOfExpertise();
        // db.addExpertise();
        int output = a.checkAreaOfExpExist("Osteopathy", 0);
        assertEquals(1, output);

    }
}
