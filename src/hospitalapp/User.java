public class User {
    private int userTypeId;

    public User(int id) {
        this.setTypeId(id);
    }

    public User() {
        this.userTypeId = -1;
    }

    public int getTypeId() {
        return this.userTypeId;
    }

    public void setTypeId(int id) {
        this.userTypeId = id;
    }

    public String toString() {

        return "";
    }

}
