public class Patient {
    private int uniqueId;
    private String patientName;
    private String telephone;

    public Patient(String name, int uniqueId, String telephone) {
        this.setName(name);
        this.setId(uniqueId);
        this.setPhoneNumber(telephone);
    }

    public Patient() {
        this.patientName = "";
        this.uniqueId = -1;
    }

    public String getName() {
        return this.patientName;
    }

    public void setName(String name) {
        this.patientName = name;
    }

    public int getId() {
        return this.uniqueId;
    }

    public void setId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getPhoneNumber() {
        return this.telephone;
    }

    public void setPhoneNumber(String telephone) {
        this.telephone = telephone;
    }

    public int validatePatient(int id, MyDatabase db) {

        String pname = "";
        for (int k = 0; k < db.patientDB.size(); k++) {
            if (id == db.patientDB.get(k).uniqueId) {
                pname = db.patientDB.get(k).getName();
                break;
            }
        }
        if (pname != "") {
            return 1;
        } else {
            return 0;
        }

    }

    public int getAppointment(int patientID, MyDatabase db) {

        String pname = "";
        for (int k = 0; k < db.patientDB.size(); k++) {
            if (patientID == db.patientDB.get(k).uniqueId) {
                pname = db.patientDB.get(k).getName();
                break;
            }
        }
        if (pname != "") {
            System.out.println("Your appointments are :");

            for (int i = 0; i < db.appointmentDB.size(); i++) {
                for (int j = 0; j < db.timetableDB.size(); j++) {
                    if ((db.appointmentDB.get(i).getUserName() == pname)
                            && (db.appointmentDB.get(i).getSlotID() == db.timetableDB.get(j).getSlotID())) {
                        System.out.println("Slot ID : " + (db.timetableDB.get(j).getSlotID()));
                        System.out.println("Treatment : " + db.timetableDB.get(j).getTreatmentName());
                        System.out.println("Physician : " + db.timetableDB.get(j).getPhysicianName());
                        System.out.println("Timeslot : " + db.timetableDB.get(j).getTimeslot());
                        System.out.println("Room : " + db.timetableDB.get(j).getRoomName());
                        System.out.println("Appointment Status : " + db.timetableDB.get(j).getAppointmentStatus());
                        System.out.println("*********************************************");
                    }
                }

            }
            return 1;
        } else {
            return 0;
        }

    }

    public String toString() {

        return "Patient Name: " + this.patientName + " Customer Phone Number " + this.telephone + " Customer unique Id "
                + this.uniqueId;
    }
}
