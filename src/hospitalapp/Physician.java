public class Physician {

    private int uniqueId;
    private String phyName;
    private String phyAddress;
    private String[] phyAreaOfExpertise;
    private int phoneNumber;
    private String consultationHours;

    public Physician() {

    }

    public Physician(int uniqueId, String phyName, String phyAddress, String[] phyAreaOfExpertise, int phoneNumber,
            String consultationHours) {

        this.uniqueId = uniqueId;
        this.phyName = phyName;
        this.phyAddress = phyAddress;
        this.phyAreaOfExpertise = phyAreaOfExpertise;
        this.phoneNumber = phoneNumber;
        this.consultationHours = consultationHours;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getPhyName() {
        return phyName;
    }

    public void setPhyName(String phyName) {
        this.phyName = phyName;
    }

    public String getPhyAddress() {
        return phyAddress;
    }

    public void setPhyAddress(String phyAddress) {
        this.phyAddress = phyAddress;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getConsultationHours() {
        return consultationHours;
    }

    public void setConsultationHours(String consultationHours) {
        this.consultationHours = consultationHours;
    }

    public String[] getPhyAreaOfExpertise() {
        return phyAreaOfExpertise;
    }

    public void setPhyAreaOfExpertise(String[] phyAreaOfExpertise) {
        this.phyAreaOfExpertise = phyAreaOfExpertise;
    }

    public String toString() {
        return "ID" + this.uniqueId + "Physician Name: " + this.phyName + " Physician Address: " + this.phyAddress
                + "Area of Expertise" + this.phyAreaOfExpertise + "Phone Number" + this.phoneNumber
                + "Consultation Hours" + this.consultationHours;
    }
}
