public class Appointment {
    private String userName;
    private String userType;

    private int slotID;

    public Appointment(String userName, String userType, int slotID) {
        this.userName = userName;
        this.userType = userType;
        this.slotID = slotID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public Appointment() {
        this.userName = "";
        this.slotID = 0;
    }
}