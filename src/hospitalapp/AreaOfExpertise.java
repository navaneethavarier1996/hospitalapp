public class AreaOfExpertise {
    private String exp;

    public AreaOfExpertise(String e) {
        this.setExp(e);
    }

    public AreaOfExpertise() {
        this.exp = "";
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String toString() {
        return "Expertise" + this.exp;
    }

    public int checkAreaOfExpExist(String exp, int index) {
        MyDatabase db = new MyDatabase();
        for (int i = 0; i < db.physiciansDB.get(index).getPhyAreaOfExpertise().length; i++) {
            if (db.physiciansDB.get(index).getPhyAreaOfExpertise()[i].equals(exp)) {
                return 1;
            }
        }
        return -1;
    }
}
