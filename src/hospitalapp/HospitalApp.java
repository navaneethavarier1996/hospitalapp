
import java.util.ArrayList;
import java.util.Scanner;

public class HospitalApp extends MyDatabase {

    public MyDatabase db = new MyDatabase();
    int tempFlag1 = 1, tempFlag2 = 1;

    // Function which is being called in the main method. It is mainly responsible
    // for maintaining user flow.
    public User userDetails() {
        int userType = -1; // 1 - Old patient, 2 - New patient, 3 - Visitor
        int patientType = -1;
        Scanner input = new Scanner(System.in);
        User user = new User();
        char flag = 'Y';
        do {
            System.out.println("Welcome to PSIC. Please select one of the below options");
            System.out.println("1. Login as a Visitor");
            System.out.println("2. Login as a Patient or Register as a new Patient");
            System.out.println("3. Generate Report-1");
            System.out.println("4. Generate Report-2");
            System.out.println("5. Exit");

            userType = input.nextInt();

            if (userType == 1) {
                user.setTypeId(3);
                System.out.println("Please enter your name");
                String visitorName = input.next();
                int x = searchPhy(visitorName, 3); // Function call to allow visitors to search for consultation slots
                if (x == -1) {
                    continue;
                }

            } else if (userType == 2) {
                System.out.println("Are you a registered patient?");
                System.out.println("1. Registered patient");
                System.out.println("2. New patient");
                patientType = input.nextInt();
                Patient p = new Patient();

                if (patientType == 1) {
                    System.out.println("Please enter your patient ID :");
                    int patientID = input.nextInt();
                    int valid = p.validatePatient(patientID, db); // Validates whether the user exists already
                    if (valid == 1) {
                        System.out.println("Please select one of the following options:");
                        System.out.println("1. Book an appointment");
                        System.out.println("2. Cancel an appointment");
                        System.out.println("3. View appointments");
                        System.out.println("4. Check in to an appointment");
                        int option = input.nextInt();

                        registeredPatientOperations(option, patientID); // All operations which can be done by
                                                                        // registered patients is defined in this
                                                                        // method.
                    } else {
                        System.out.println("Please enter a valid id");
                    }

                } else if (patientType == 2) {
                    System.out.println("Please enter the below details to register as a patient:-");
                    // Function to enable a new user to register as a patient
                    patientDetailsInput();
                } else {
                    System.out.println("Please enter a valid choice.");
                }
                user.setTypeId(patientType);

            } else if (userType == 3) {
                System.out.println("************** REPORT - 1 ***************************");
                System.out.println("All the treatment appointments for the last 4 weeks are as follows:-");
                for (int i = 0; i < db.appointmentDB.size(); i++) {
                    if (db.appointmentDB.get(i).getUserType() == "Patient") {
                        for (int j = 0; j < db.timetableDB.size(); j++) {
                            if (db.timetableDB.get(j).getSlotID() == db.appointmentDB.get(i).getSlotID()) {
                                System.out.println("Physician Name : " + db.timetableDB.get(j).getPhysicianName());
                                System.out.println("Treatment Name : " + db.timetableDB.get(j).getTreatmentName());
                                System.out.println("Patient Name : " + db.appointmentDB.get(i).getUserName());
                                System.out.println("Time : " + db.timetableDB.get(j).getTimeslot());
                                System.out.println("Room : " + db.timetableDB.get(j).getRoomName());
                                System.out.println("******************************************");
                            }
                        }

                    }

                }
                System.out.println("All the visitor appointments for the last 4 weeks are as follows:-");
                for (int i = 0; i < db.appointmentDB.size(); i++) {
                    if (db.appointmentDB.get(i).getUserType() == "Visitor") {
                        for (int j = 0; j < db.timetableDB.size(); j++) {
                            if (db.timetableDB.get(j).getSlotID() == db.appointmentDB.get(i).getSlotID()) {
                                System.out.println("Physician Name : " + db.timetableDB.get(j).getPhysicianName());

                                System.out.println("Time : " + db.timetableDB.get(j).getTimeslot());
                                System.out.println(
                                        "NOTES - The name of the visitor is " + db.appointmentDB.get(i).getUserName());
                                System.out.println("******************************************");
                            }
                        }
                    }
                }
            } else if (userType == 4) {
                System.out.println("******************** REPORT - 2 *************************8");
                for (int i = 0; i < db.patientDB.size(); i++) {
                    System.out.println("All treatment appointments of " + db.patientDB.get(i).getName() + " are:-");
                    int k = 1;
                    int flag1 = 0;
                    for (int j = 0; j < db.appointmentDB.size(); j++) {
                        if (db.patientDB.get(i).getName().equals(db.appointmentDB.get(j).getUserName())) {
                            int id = db.appointmentDB.get(j).getSlotID();

                            flag1 = flag1 + 1;
                            System.out.println(k + ". Appointment ID - " + db.timetableDB.get(id).getSlotID());

                            System.out.println("Physician Name - " + db.timetableDB.get(id).getPhysicianName());

                            System.out.println("Treatment Name - " + db.timetableDB.get(id).getTreatmentName());

                            System.out.println("Time Slot - " + db.timetableDB.get(id).getTimeslot());

                            System.out.println("Room - " + db.timetableDB.get(id).getRoomName());

                            System.out.println("Status - " + db.timetableDB.get(id).getAppointmentStatus());

                            k = k + 1;

                            break;

                        } else {

                            flag1 = 0;
                            continue;
                        }
                    }
                    if (flag1 == 0) {
                        System.out.println("No appointments booked.");
                    }
                    System.out.println("****************************************************");
                }
            } else if (userType == 5) {
                System.exit(0);
            }
        } while (flag != 'N');
        return user;

    }

    public void registeredPatientOperations(int option, int patientID) {
        Scanner input = new Scanner(System.in);

        switch (option) {

            case 1:
                for (int i = 0; i < db.patientDB.size(); i++) {
                    if (db.patientDB.get(i).getId() == patientID) {
                        searchPhy(db.patientDB.get(i).getName(), patientID);
                        break;
                    } else {
                        if (i == db.patientDB.size() - 1)
                            System.out.println("Please enter a valid id");
                        else
                            continue;
                    }
                }

                break;

            case 2:
                System.out.println("Enter the slot ID you wish to cancel");
                int id = input.nextInt();

                Timetable t = new Timetable();
                t.validateSlotID(2, db);

                Timetable obj = new Timetable(db.timetableDB.get(id).getSlotID(), db.timetableDB.get(id).getSlotType(),
                        db.timetableDB.get(id).getPhysicianName(), db.timetableDB.get(id).getTreatmentName(),
                        db.timetableDB.get(id).getTimeslot(), db.timetableDB.get(id).getRoomName(), "Cancelled");

                db.timetableDB.set(id, obj);
                System.out.println(db.timetableDB.get(id).getAppointmentStatus());
                System.out.println("Slot ID " + id + " has been cancelled successfully.");
                break;

            case 3:
                Patient p = new Patient();
                p.getAppointment(patientID, db);
                break;

            case 4:
                System.out.println("Enter the slot ID you wish to attend");
                int id1 = input.nextInt();

                Timetable obj1 = new Timetable(db.timetableDB.get(id1).getSlotID(),
                        db.timetableDB.get(id1).getSlotType(), db.timetableDB.get(id1).getPhysicianName(),
                        db.timetableDB.get(id1).getTreatmentName(), db.timetableDB.get(id1).getTimeslot(),
                        db.timetableDB.get(id1).getRoomName(), "Attended");

                db.timetableDB.set(id1, obj1);

                System.out.println("Checked in to slot ID " + id1 + " successfully.");
                break;

            default:
                System.out.println("Please enter a valid response");
        }
    }

    public int searchPhy(String name, int typeOfUser) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please select one of the below options to search for a physician");
        System.out.println("1. Name");
        System.out.println("2. Expertise");
        int choice = input.nextInt();

        switch (choice) {
            case 1:
                System.out.println("Choose one of the physicians:");
                for (int i = 0; i < db.physiciansDB.size(); i++) {
                    System.out.println((i + 1) + ". " + db.physiciansDB.get(i).getPhyName());
                }
                int searchType = input.nextInt();
                String phyName = db.physiciansDB.get(searchType - 1).getPhyName();

                System.out.println("Please select one of the available timeslots:-");
                int k1 = 0, k2 = 0;
                if (typeOfUser == 3) {

                    for (int j = 0; j < db.timetableDB.size(); j++) {
                        if ((db.timetableDB.get(j).getPhysicianName() == phyName)
                                && ((db.timetableDB.get(j).getAppointmentStatus() == "Available")
                                        || (db.timetableDB.get(j).getAppointmentStatus() == "Cancelled"))
                                && (db.timetableDB.get(j).getSlotType() == "Consultation")) {
                            System.out.println(j + ". " + "Treatment : " + db.timetableDB.get(j).getTreatmentName());
                            System.out.println("Timeslot : " + db.timetableDB.get(j).getTimeslot());
                            System.out.println("Room : " + db.timetableDB.get(j).getRoomName());
                            System.out.println("**************************************");
                            k1++;
                        }
                    }
                    if (k1 == 0) {
                        System.out.println("Sorry no timeslots available");
                        tempFlag1 = -1;
                    }
                } else {
                    for (int j = 0; j < db.timetableDB.size(); j++) {
                        if ((db.timetableDB.get(j).getPhysicianName() == phyName)
                                && ((db.timetableDB.get(j).getAppointmentStatus() == "Available")
                                        || (db.timetableDB.get(j).getAppointmentStatus() == "Cancelled"))
                                && (db.timetableDB.get(j).getSlotType() == "Treatment")) {
                            System.out.println(j + ". " + "Treatment : " + db.timetableDB.get(j).getTreatmentName());
                            System.out.println("Timeslot : " + db.timetableDB.get(j).getTimeslot());
                            System.out.println("Room : " + db.timetableDB.get(j).getRoomName());
                            System.out.println("**************************************");
                            k2++;
                        }
                    }
                    if (k2 == 0) {
                        System.out.println("Sorry no timeslots available");
                        tempFlag2 = -1;
                    }
                }

                System.out.println("Enter your choice");
                int ch = input.nextInt();

                Timetable obj = new Timetable(db.timetableDB.get(ch).getSlotID(), db.timetableDB.get(ch).getSlotType(),
                        db.timetableDB.get(ch).getPhysicianName(), db.timetableDB.get(ch).getTreatmentName(),
                        db.timetableDB.get(ch).getTimeslot(), db.timetableDB.get(ch).getRoomName(), "Booked");
                String userType = "";
                if (typeOfUser == 3) {
                    userType = "Visitor";

                } else {
                    userType = "Patient";

                }

                Appointment app = new Appointment(name, userType, ch);
                db.timetableDB.set(ch, obj);
                db.appointmentDB.add(app);
                System.out.println("Your booking has been confirmed.");

                if (tempFlag1 == -1 || tempFlag2 == -1) {
                    return -1;
                }

                else {
                    return 1;

                }

            case 2:
                System.out.println("Choose one of the area of expertise");
                for (int i = 0; i < db.areaOfExpertise.size(); i++) {

                    System.out.println((i + 1) + ". " + db.areaOfExpertise.get(i).getExp());
                }
                searchType = input.nextInt();

                String e = db.areaOfExpertise.get(searchType - 1).getExp();
                System.out.println("We have the below treatments available under this area of expertise.");
                System.out.println("Please select one of the available slots");
                System.out.println("Area of expertise" + e);

                AreaOfExpertise a = new AreaOfExpertise();
                String t1 = "", t2 = "";

                if (e == "Osteopathy") {
                    t1 = "Mobilisation of spine and joints";
                    t2 = "Neural Mobilisation";
                }
                if (e == "Physiotherapy") {
                    t1 = "Massage";
                    t2 = "Acupuncture";

                }
                if (e == "Rehabilitation") {
                    t1 = "Gym Rehabilitation";
                    t2 = "Pool Rehabilitation";
                }

                for (int j = 0; j < db.physiciansDB.size(); j++) {

                    int res = a.checkAreaOfExpExist(e, j);

                    if (res == 1) {
                        for (int k = 0; k < db.timetableDB.size(); k++) {
                            if (typeOfUser == 3) {
                                if ((db.timetableDB.get(k).getPhysicianName() == db.physiciansDB.get(j).getPhyName())
                                        && (db.timetableDB.get(k).getSlotType() == "Consultation")
                                        && ((db.timetableDB.get(k).getTreatmentName() == t1)
                                                || (db.timetableDB.get(k).getTreatmentName() == t2))) {
                                    System.out.println((k + 1) + ". " + "Physician Name : "
                                            + db.timetableDB.get(k).getPhysicianName());
                                    System.out.println("Treatment : " + db.timetableDB.get(k).getTreatmentName());
                                    System.out.println("Timeslot : " + db.timetableDB.get(k).getTimeslot());
                                    System.out.println("Room : " + db.timetableDB.get(k).getRoomName());
                                    System.out.println("***********************************");

                                }
                            } else {
                                if ((db.timetableDB.get(k).getPhysicianName() == db.physiciansDB.get(j).getPhyName())
                                        && ((db.timetableDB.get(k).getSlotType() == "Treatment"))
                                        && ((db.timetableDB.get(k).getTreatmentName() == t1)
                                                || (db.timetableDB.get(k).getTreatmentName() == t2))) {
                                    System.out.println((k + 1) + ". " + "Physician Name : "
                                            + db.timetableDB.get(k).getPhysicianName());
                                    System.out.println("Treatment : " + db.timetableDB.get(k).getTreatmentName());
                                    System.out.println("Timeslot : " + db.timetableDB.get(k).getTimeslot());
                                    System.out.println("Room : " + db.timetableDB.get(k).getRoomName());
                                    System.out.println("***********************************");
                                }
                            }

                        }

                    }
                }

                int choice1 = input.nextInt();
                choice1 = choice1 - 1;
                Timetable obj1 = new Timetable(db.timetableDB.get(choice1).getSlotID(),
                        db.timetableDB.get(choice1).getSlotType(), db.timetableDB.get(choice1).getPhysicianName(),
                        db.timetableDB.get(choice1).getTreatmentName(), db.timetableDB.get(choice1).getTimeslot(),
                        db.timetableDB.get(choice1).getRoomName(), "Booked");

                String userType1 = "";
                if (typeOfUser == 3) {
                    userType1 = "Visitor";

                } else {
                    userType1 = "Patient";

                }
                Appointment app1 = new Appointment(name, userType1, db.timetableDB.get(choice).getSlotID());
                db.timetableDB.set(choice, obj1);
                db.appointmentDB.add(app1);
                System.out.println("Your booking has been confirmed.");

                break;

            default:
                System.out.println("Please enter a valid input");
        }
        return 0;
    }

    // Function to check if name entered is valid or not i.e. it should only contain
    // characaters.
    public int nameValidity(String name) {
        char ch;
        for (int i = 0; i < name.length(); i++) {
            ch = name.charAt(i);
            if ((((int) ch >= 65) && ((int) ch <= 90)) || (((int) ch >= 97) && ((int) ch <= 122))) {
                //
            } else {
                return 0;
            }
        }
        return 1;
    }

    // Function to check if phone number entered is valid or not i.e. it should only
    // contain digits.
    public int phoneNumberValidity(String no) {
        char ch;
        for (int i = 0; i < no.length(); i++) {
            ch = no.charAt(i);
            if (((int) ch >= 48) && ((int) ch <= 57)) {
                //
            } else {
                return 0;
            }
        }
        return 1;
    }

    // Method to enable new users to register as a patient
    public Patient patientDetailsInput() {
        String patientName = "";
        String telephone = "";
        int uniqueId = -1;

        Scanner input = new Scanner(System.in);
        Patient patient = new Patient();

        System.out.println("Enter your Name: ");
        patientName = input.nextLine();
        int v1 = nameValidity(patientName);
        System.out.println("Enter your Phone Number: ");
        telephone = input.nextLine();
        int v2 = phoneNumberValidity(telephone);

        uniqueId = db.patientDB.size() + 1;
        System.out.println(
                "You have been registered successfully. Your ID is " + uniqueId + ". Please use this ID to login.");

        if ((v1 == 1) && (v2 == 1)) {
            patient.setName(patientName);
            patient.setPhoneNumber(telephone);
            patient.setId(uniqueId);
            db.addPatient(patient);
        }

        return patient;
    }

    public static void main(String[] args) {

        HospitalApp hp = new HospitalApp();
        System.out.println(hp.userDetails());
    }

}
