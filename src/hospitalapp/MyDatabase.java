
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MyDatabase {
        public ArrayList<Physician> physiciansDB = new ArrayList<>();
        public ArrayList<Patient> patientDB = new ArrayList<>();
        public ArrayList<Timetable> timetableDB = new ArrayList<>();
        public ArrayList<AreaOfExpertise> areaOfExpertise = new ArrayList<>();
        public ArrayList<Appointment> appointmentDB = new ArrayList<>();

        public MyDatabase() {
                Patient p1 = new Patient("John", 1, "12345654");
                Patient p2 = new Patient("James", 2, "12345653");
                Patient p3 = new Patient("Joana", 3, "12345656");
                Patient p4 = new Patient("George", 4, "12345654");
                Patient p5 = new Patient("Adam", 5, "453656");
                Patient p6 = new Patient("Ann", 6, "4654675");
                Patient p7 = new Patient("Julie", 7, "68787");
                Patient p8 = new Patient("Jasmin", 8, "4534645");
                Patient p9 = new Patient("Edward", 9, "56654");
                Patient p10 = new Patient("Daniel", 10, "6575568");
                Patient p11 = new Patient("Susan", 11, "6436456");
                Patient p12 = new Patient("Jane", 12, "65465");
                Patient p13 = new Patient("Martha", 13, "86787");
                Patient p14 = new Patient("Monica", 14, "2324432");
                Patient p15 = new Patient("Rachel", 15, "5765876");
                Physician phy1 = new Physician(1, "Nandana", "Hatfield", new String[] { "Physiotherapy", "Osteopathy" },
                                1123245, "Monday 7:00-9:00");
                Physician phy2 = new Physician(2, "Hari", "Surrey", new String[] { "Osteopathy", "Rehabilitation" },
                                112324455, "Tuesday 7:00-9:00");
                Physician phy3 = new Physician(3, "Satish", "London",
                                new String[] { "Rehabilitation", "Physiotherapy" }, 1123247865, "Wednesday 7:00-9:00");
                Physician phy4 = new Physician(4, "Sai", "Edinburgh", new String[] { "Rehabilitation" }, 11235245,
                                "Thursday 7:00-9:00");
                Physician phy5 = new Physician(5, "Tatiana", "Hertfordshire", new String[] { "Osteopathy" }, 115675245,
                                "Friday 7:00-9:00");

                AreaOfExpertise osteopathy = new AreaOfExpertise("Osteopathy");
                AreaOfExpertise physiotherapy = new AreaOfExpertise("Physiotherapy");
                AreaOfExpertise rehabilitation = new AreaOfExpertise("Rehabilitation");

                // Week 1 Consultation slots for physician name = Nandana
                addTimetableObj(new Timetable(1, "Consultation", "Nandana", "Massage", "03-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(2, "Consultation", "Nandana", "Massage", "03-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(3, "Consultation", "Nandana", "Massage", "03-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(4, "Consultation", "Nandana", "Massage", "03-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 2 Consultation slots for physician name = Nandana
                addTimetableObj(new Timetable(5, "Consultation", "Nandana", "Acupuncture", "10-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(6, "Consultation", "Nandana", "Acupuncture", "10-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(7, "Consultation", "Nandana", "Acupuncture", "10-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(8, "Consultation", "Nandana", "Acupuncture", "10-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 3 Consultation slots for physician name = Nandana
                addTimetableObj(new Timetable(9, "Consultation", "Nandana", "Mobilisation of spine and joints",
                                "17-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(10, "Consultation", "Nandana", "Mobilisation of spine and joints",
                                "17-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(11, "Consultation", "Nandana", "Mobilisation of spine and joints",
                                "17-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(12, "Consultation", "Nandana", "Mobilisation of spine and joints",
                                "17-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 4 Consultation slots for physician name = Nandana
                addTimetableObj(new Timetable(13, "Consultation", "Nandana", "Neural Mobilisation",
                                "24-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(14, "Consultation", "Nandana", "Neural Mobilisation",
                                "24-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(15, "Consultation", "Nandana", "Neural Mobilisation",
                                "24-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(16, "Consultation", "Nandana", "Neural Mobilisation",
                                "24-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 1 Consultation slots for physician name = Hari
                addTimetableObj(new Timetable(17, "Consultation", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(18, "Consultation", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(19, "Consultation", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(20, "Consultation", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 2 Consultation slots for physician name = Hari
                addTimetableObj(new Timetable(21, "Consultation", "Hari", "Neural Mobilisation", "11-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(22, "Consultation", "Hari", "Neural Mobilisation", "11-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(23, "Consultation", "Hari", "Neural Mobilisation", "11-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(24, "Consultation", "Hari", "Neural Mobilisation", "11-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 3 Consultation slots for physician name = Hari
                addTimetableObj(new Timetable(25, "Consultation", "Hari", "Pool Rehabilitation", "18-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(26, "Consultation", "Hari", "Pool Rehabilitation", "18-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(27, "Consultation", "Hari", "Pool Rehabilitation", "18-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(28, "Consultation", "Hari", "Pool Rehabilitation", "18-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 4 Consultation slots for physician name = Hari
                addTimetableObj(new Timetable(29, "Consultation", "Hari", "Gym Rehabilitation", "25-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(30, "Consultation", "Hari", "Gym Rehabilitation", "25-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(31, "Consultation", "Hari", "Gym Rehabilitation", "25-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(32, "Consultation", "Hari", "Gym Rehabilitation", "25-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 1 Consultation slots for physician name = Satish
                addTimetableObj(new Timetable(33, "Consultation", "Satish", "Pool Rehabilitation",
                                "05-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(34, "Consultation", "Satish", "Pool Rehabilitation",
                                "05-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(35, "Consultation", "Satish", "Pool Rehabilitation",
                                "05-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(36, "Consultation", "Satish", "Pool Rehabilitation",
                                "05-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 2 Consultation slots for physician name = Satish
                addTimetableObj(new Timetable(37, "Consultation", "Satish", "Gym Rehabilitation",
                                "12-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(38, "Consultation", "Satish", "Gym Rehabilitation",
                                "12-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(39, "Consultation", "Satish", "Gym Rehabilitation",
                                "12-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(40, "Consultation", "Satish", "Gym Rehabilitation",
                                "12-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 3 Consultation slots for physician name = Satish
                addTimetableObj(new Timetable(41, "Consultation", "Satish", "Massage", "19-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(42, "Consultation", "Satish", "Massage", "19-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(43, "Consultation", "Satish", "Massage", "19-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(44, "Consultation", "Satish", "Massage", "19-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 4 Consultation slots for physician name = Satish
                addTimetableObj(new Timetable(45, "Consultation", "Satish", "Acupuncture", "26-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(46, "Consultation", "Satish", "Acupuncture", "26-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(47, "Consultation", "Satish", "Acupuncture", "26-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(48, "Consultation", "Satish", "Acupuncture", "26-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 1 Consultation slots for physician name = Sai
                addTimetableObj(new Timetable(49, "Consultation", "Sai", "Pool Rehabilitation", "06-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(50, "Consultation", "Sai", "Pool Rehabilitation", "06-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(51, "Consultation", "Sai", "Pool Rehabilitation", "06-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(52, "Consultation", "Sai", "Pool Rehabilitation", "06-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 2 Consultation slots for physician name = Sai
                addTimetableObj(new Timetable(53, "Consultation", "Sai", "Pool Rehabilitation", "13-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(54, "Consultation", "Sai", "Pool Rehabilitation", "13-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(55, "Consultation", "Sai", "Pool Rehabilitation", "13-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(56, "Consultation", "Sai", "Pool Rehabilitation", "13-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 3 Consultation slots for physician name = Sai
                addTimetableObj(new Timetable(57, "Consultation", "Sai", "Gym Rehabilitation", "20-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(58, "Consultation", "Sai", "Gym Rehabilitation", "20-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(59, "Consultation", "Sai", "Gym Rehabilitation", "20-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(60, "Consultation", "Sai", "Gym Rehabilitation", "20-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 4 Consultation slots for physician name = Sai
                addTimetableObj(new Timetable(61, "Consultation", "Sai", "Gym Rehabilitation", "27-05-2021 7:00-7:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(62, "Consultation", "Sai", "Gym Rehabilitation", "27-05-2021 7:30-8:00",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(63, "Consultation", "Sai", "Gym Rehabilitation", "27-05-2021 8:00-8:30",
                                "Consultation Room", "Available"));

                addTimetableObj(new Timetable(64, "Consultation", "Sai", "Gym Rehabilitation", "27-05-2021 8:30-9:00",
                                "Consultation Room", "Available"));

                // Week 1 Consultation slots for physician name = Tatiana
                addTimetableObj(new Timetable(65, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "07-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(66, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "07-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(67, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "07-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(68, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "07-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 2 Consultation slots for physician name = Tatiana
                addTimetableObj(new Timetable(69, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "14-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(70, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "14-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(71, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "14-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(72, "Consultation", "Tatiana", "Mobilisation of spine and joints",
                                "14-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 3 Consultation slots for physician name = Tatiana
                addTimetableObj(new Timetable(73, "Consultation", "Tatiana", "Neural Mobilisation",
                                "21-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(74, "Consultation", "Tatiana", "Neural Mobilisation",
                                "21-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(75, "Consultation", "Tatiana", "Neural Mobilisation",
                                "21-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(76, "Consultation", "Tatiana", "Neural Mobilisation",
                                "21-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Week 4 Consultation slots for physician name = Tatiana
                addTimetableObj(new Timetable(77, "Consultation", "Tatiana", "Neural Mobilisation",
                                "28-05-2021 7:00-7:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(78, "Consultation", "Tatiana", "Neural Mobilisation",
                                "28-05-2021 7:30-8:00", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(79, "Consultation", "Tatiana", "Neural Mobilisation",
                                "28-05-2021 8:00-8:30", "Consultation Room", "Available"));

                addTimetableObj(new Timetable(80, "Consultation", "Tatiana", "Neural Mobilisation",
                                "28-05-2021 8:30-9:00", "Consultation Room", "Available"));

                // Treatment Appointments

                // Week - 1 Treatment appointments for physician name = Tatiana

                addTimetableObj(new Timetable(81, "Treatment", "Tatiana", "Neural Mobilisation", "03-05-2021 7:00-9:00",
                                "Consultation Room - A", "Available"));

                addTimetableObj(new Timetable(82, "Treatment", "Tatiana", "Neural Mobilisation",
                                "03-05-2021 12:00-2:00", "Consultation Room - A", "Available"));

                addTimetableObj(new Timetable(83, "Treatment", "Tatiana", "Neural Mobilisation",
                                "03-05-2021 15:00-17:00", "Consultation Room - A", "Available"));

                // Week - 2 Treatment appointments for physician name = Tatiana

                addTimetableObj(new Timetable(84, "Treatment", "Tatiana", "Neural Mobilisation", "10-05-2021 7:00-9:00",
                                "Consultation Room - B", "Available"));

                addTimetableObj(new Timetable(85, "Treatment", "Tatiana", "Neural Mobilisation",
                                "10-05-2021 12:00-2:00", "Consultation Room - B", "Available"));

                addTimetableObj(new Timetable(86, "Treatment", "Tatiana", "Neural Mobilisation",
                                "10-05-2021 15:00-17:00", "Consultation Room - B", "Available"));

                // Week - 3 Treatment appointments for physician name = Tatiana

                addTimetableObj(new Timetable(87, "Treatment", "Tatiana", "Mobilisation of spine and joints",
                                "17-05-2021 7:00-9:00", "Consultation Room - C", "Available"));

                addTimetableObj(new Timetable(88, "Treatment", "Tatiana", "Mobilisation of spine and joints",
                                "17-05-2021 12:00-2:00", "Consultation Room - C", "Available"));

                addTimetableObj(new Timetable(89, "Treatment", "Tatiana", "Mobilisation of spine and joints",
                                "17-05-2021 15:00-17:00", "Consultation Room - C", "Available"));

                // Week - 4 Treatment appointments for physician name = Tatiana

                addTimetableObj(new Timetable(90, "Treatment", "Tatiana", "Mobilisation of spine and joints",
                                "24-05-2021 7:00-9:00", "Consultation Room - A", "Available"));

                addTimetableObj(new Timetable(91, "Treatment", "Tatiana", "Mobilisation of spine and joints",
                                "24-05-2021 12:00-2:00", "Consultation Room - A", "Available"));

                addTimetableObj(new Timetable(92, "Treatment", "Tatiana", "Mobilisation of spine and joints",
                                "24-05-2021 15:00-17:00", "Consultation Room - A", "Available"));

                // Week - 1 Treatment appointments for physician name = Sai

                addTimetableObj(new Timetable(93, "Treatment", "Sai", "Pool Rehabilitation", "04-05-2021 7:00-9:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(94, "Treatment", "Sai", "Pool Rehabilitation", "04-05-2021 12:00-2:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(95, "Treatment", "Sai", "Pool Rehabilitation", "04-05-2021 15:00-17:00",
                                "Swimming Pool - A", "Available"));

                // Week - 2 Treatment appointments for physician name = Sai

                addTimetableObj(new Timetable(96, "Treatment", "Sai", "Gym Rehabilitation", "11-05-2021 7:00-9:00",
                                "Gym - B", "Available"));

                addTimetableObj(new Timetable(97, "Treatment", "Sai", "Gym Rehabilitation", "11-05-2021 12:00-2:00",
                                "Gym - B", "Available"));

                addTimetableObj(new Timetable(98, "Treatment", "Sai", "Gym Rehabilitation", "11-05-2021 15:00-17:00",
                                "Gym - B", "Available"));

                // Week - 3 Treatment appointments for physician name = Sai

                addTimetableObj(new Timetable(99, "Treatment", "Sai", "Gym Rehabilitation", "18-05-2021 7:00-9:00",
                                "Gym - C", "Available"));

                addTimetableObj(new Timetable(100, "Treatment", "Sai", "Gym Rehabilitation", "18-05-2021 12:00-2:00",
                                "Gym - C", "Available"));

                addTimetableObj(new Timetable(101, "Treatment", "Sai", "Gym Rehabilitation", "18-05-2021 15:00-17:00",
                                "Gym - C", "Available"));

                // Week - 4 Treatment appointments for physician name = Sai

                addTimetableObj(new Timetable(102, "Treatment", "Sai", "Pool Rehabilitation", "25-05-2021 7:00-9:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(103, "Treatment", "Sai", "Pool Rehabilitation", "25-05-2021 12:00-2:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(104, "Treatment", "Sai", "Pool Rehabilitation", "25-05-2021 15:00-17:00",
                                "Swimming Pool - A", "Available"));

                // Week - 1 Treatment appointments for physician name = Hari

                addTimetableObj(new Timetable(105, "Treatment", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 7:00-9:00", "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(106, "Treatment", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 12:00-2:00", "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(107, "Treatment", "Hari", "Mobilisation of spine and joints",
                                "04-05-2021 15:00-17:00", "Consultation Room  - A", "Available"));

                // Week - 2 Treatment appointments for physician name = Hari

                addTimetableObj(new Timetable(108, "Treatment", "Hari", "Gym Rehabilitation", "12-05-2021 7:00-9:00",
                                "Gym - B", "Available"));

                addTimetableObj(new Timetable(109, "Treatment", "Hari", "Gym Rehabilitation", "12-05-2021 12:00-2:00",
                                "Gym - B", "Available"));

                addTimetableObj(new Timetable(110, "Treatment", "Hari", "Gym Rehabilitation", "12-05-2021 15:00-17:00",
                                "Gym - B", "Available"));

                // Week - 3 Treatment appointments for physician name = Hari

                addTimetableObj(new Timetable(111, "Treatment", "Hari", "Neural Mobilisation", "19-05-2021 7:00-9:00",
                                "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(112, "Treatment", "Hari", "Neural Mobilisation", "19-05-2021 12:00-2:00",
                                "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(113, "Treatment", "Hari", "Neural Mobilisation", "19-05-2021 15:00-17:00",
                                "Consultation Room - C", "Available"));

                // Week - 4 Treatment appointments for physician name = Hari

                addTimetableObj(new Timetable(114, "Treatment", "Hari", "Pool Rehabilitation", "26-05-2021 7:00-9:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(115, "Treatment", "Hari", "Pool Rehabilitation", "26-05-2021 12:00-2:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(116, "Treatment", "Hari", "Pool Rehabilitation", "26-05-2021 15:00-17:00",
                                "Swimming Pool - A", "Available"));

                // Week - 1 Treatment appointments for physician name = Satish

                addTimetableObj(new Timetable(117, "Treatment", "Satish", "Massage", "05-05-2021 7:00-9:00",
                                "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(118, "Treatment", "Satish", "Massage", "05-05-2021 12:00-2:00",
                                "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(119, "Treatment", "Satish", "Massage", "05-05-2021 15:00-17:00",
                                "Consultation Room  - A", "Available"));

                // Week - 2 Treatment appointments for physician name = Satish

                addTimetableObj(new Timetable(120, "Treatment", "Satish", "Gym Rehabilitation", "13-05-2021 7:00-9:00",
                                "Gym - B", "Available"));

                addTimetableObj(new Timetable(121, "Treatment", "Satish", "Gym Rehabilitation", "13-05-2021 12:00-2:00",
                                "Gym - B", "Available"));

                addTimetableObj(new Timetable(122, "Treatment", "Satish", "Gym Rehabilitation",
                                "13-05-2021 15:00-17:00", "Gym - B", "Available"));

                // Week - 3 Treatment appointments for physician name = Satish

                addTimetableObj(new Timetable(123, "Treatment", "Satish", "Acupuncture", "20-05-2021 7:00-9:00",
                                "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(124, "Treatment", "Satish", "Acupuncture", "20-05-2021 12:00-2:00",
                                "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(125, "Treatment", "Satish", "Acupuncture", "20-05-2021 15:00-17:00",
                                "Consultation Room - C", "Available"));

                // Week - 4 Treatment appointments for physician name = Satish

                addTimetableObj(new Timetable(126, "Treatment", "Satish", "Pool Rehabilitation", "27-05-2021 7:00-9:00",
                                "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(127, "Treatment", "Satish", "Pool Rehabilitation",
                                "27-05-2021 12:00-2:00", "Swimming Pool - A", "Available"));

                addTimetableObj(new Timetable(128, "Treatment", "Satish", "Pool Rehabilitation",
                                "27-05-2021 15:00-17:00", "Swimming Pool - A", "Available"));

                // Week - 1 Treatment appointments for physician name = Nandana

                addTimetableObj(new Timetable(129, "Treatment", "Nandana", "Massage", "06-05-2021 7:00-9:00",
                                "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(130, "Treatment", "Nandana", "Massage", "06-05-2021 12:00-2:00",
                                "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(131, "Treatment", "Nandana", "Massage", "06-05-2021 15:00-17:00",
                                "Consultation Room  - A", "Available"));

                // Week - 2 Treatment appointments for physician name = Nandana

                addTimetableObj(new Timetable(132, "Treatment", "Nandana", "Mobilisation of spine and joints",
                                "14-05-2021 7:00-9:00", "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(133, "Treatment", "Nandana", "Mobilisation of spine and joints",
                                "14-05-2021 12:00-2:00", "Consultation Room  - A", "Available"));

                addTimetableObj(new Timetable(134, "Treatment", "Nandana", "Mobilisation of spine and joints",
                                "14-05-2021 15:00-17:00", "Consultation Room  - A", "Available"));

                // Week - 3 Treatment appointments for physician name = Nandana

                addTimetableObj(new Timetable(135, "Treatment", "Nandana", "Acupuncture", "21-05-2021 7:00-9:00",
                                "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(136, "Treatment", "Nandana", "Acupuncture", "21-05-2021 12:00-2:00",
                                "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(137, "Treatment", "Nandana", "Acupuncture", "21-05-2021 15:00-17:00",
                                "Consultation Room - C", "Available"));

                // Week - 4 Treatment appointments for physician name = Nandana

                addTimetableObj(new Timetable(138, "Treatment", "Nandana", "Neural Mobilisation",
                                "28-05-2021 7:00-9:00", "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(139, "Treatment", "Nandana", "Neural Mobilisation",
                                "28-05-2021 12:00-2:00", "Consultation Room  - C", "Available"));

                addTimetableObj(new Timetable(140, "Treatment", "Nandana", "Neural Mobilisation",
                                "28-05-2021 15:00-17:00", "Consultation Room  - C", "Available"));

                patientDB.add(p1);
                patientDB.add(p2);
                patientDB.add(p3);
                patientDB.add(p4);
                patientDB.add(p5);
                patientDB.add(p6);
                patientDB.add(p7);
                patientDB.add(p8);
                patientDB.add(p9);
                patientDB.add(p10);
                patientDB.add(p11);
                patientDB.add(p12);
                patientDB.add(p13);
                patientDB.add(p14);
                patientDB.add(p15);
                physiciansDB.add(phy1);
                physiciansDB.add(phy2);
                physiciansDB.add(phy3);
                physiciansDB.add(phy4);
                physiciansDB.add(phy5);

                areaOfExpertise.add(osteopathy);
                areaOfExpertise.add(physiotherapy);
                areaOfExpertise.add(rehabilitation);

        }

        public ArrayList addPatient(Patient p) {
                patientDB.add(p);

                return patientDB;
        }

        public ArrayList addPhysician(Physician p) {

                physiciansDB.add(p);
                return physiciansDB;
        }

        public ArrayList getPhysicians() {
                return physiciansDB;
        }

        public ArrayList getExpertise() {
                return areaOfExpertise;
        }

        public void addTimetableObj(Timetable t) {
                this.timetableDB.add(t);
        }

        public ArrayList addAppointment() {
                return appointmentDB;
        }
}
